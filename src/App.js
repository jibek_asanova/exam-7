import './App.css';
import Order from "./Components/Order";
import AddItem from "./Components/AddItem";
import {useState} from "react";
import burger from './assets/hamburger.png';
import fries from './assets/french-fries.png';
import salad from './assets/salad.png';
import coffee from './assets/coffee-cup.png';
import tea from './assets/tea.png';

export const prices = {
    'Hamburger' : 90,
    'Cheeseburger' : 100,
    'Salad' : 100,
    'Fries' : 60,
    'Coffee' : 25,
    'Tea' : 25
};

const App = () => {

    const menu = [
        {name: 'Hamburger', price: 90, image: burger},
        {name: 'Cheeseburger', price: 100, image: burger},
        {name: 'Salad', price: 100, image: salad},
        {name: 'Fries', price: 60, image: fries},
        {name: 'Coffee', price: 25, image: coffee},
        {name: 'Tea', price: 25, image: tea},
    ];

    const [menuItems, setMenuItems] = useState([
        {name: 'Hamburger', count: 0, id: 1},
        {name: 'Cheeseburger', count: 0, id: 2},
        {name: 'Salad',count: 0, id: 3},
        {name: 'Fries', count: 0, id: 4},
        {name: 'Coffee', count: 0, id: 5},
        {name: 'Tea', count: 0, id: 6},
    ]);

    const getTotalCount = menuItems.reduce((sum, menuItems) => {
        return sum + prices[menuItems.name] * menuItems.count;
    }, 0);

    const increaseItemCount = id => {
        setMenuItems(menuItems.map(item => {
            if(item.id === id) {
                return {...item, count: item.count + 1}
            }
            return item;
        }))
    };

    const removeItem = id => {
        setMenuItems(menuItems.map(item => {
            if(item.id === id) {
                return {...item, count: item.count - 1}
            }
            return item;
        }))
    };

    const addItem = menuItems.map((el, i) => (
        <AddItem
            key={el.id}
            id={el.id}
            image={menu[i].image}
            price={menu[i].price}
            itemName={el.name}
            count={el.count}
            onRemove={() => removeItem(el.id)}
            onItemClick={() => increaseItemCount(el.id)}
        />
    ));


    return (
        <div>
            <div className="Container">
                <div className="Block OrderBlock">
                    <h2>Order Details</h2>
                    <Order
                        menu={menuItems}
                        onRemove={removeItem}
                    />
                    <p>Total : {getTotalCount}</p>
                </div>



                <div className="Block AddItemBlock">
                    <h2>AddItem</h2>
                    <div className="ItemBlock">
                        {addItem}
                    </div>

                </div>


            </div>
        </div>
    )

};

export default App;
