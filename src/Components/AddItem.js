import React from 'react';
import './AddItem.css';

const AddItem = ({itemName, onItemClick, image, price}) => {
    return (
        <div className="Item" onClick={onItemClick}>
            <img src={image} alt='itemImg' width='70px' className="click"/>
            <p>{itemName}</p>
            <p>Price: {price}</p>
        </div>
    );
};

export default AddItem;