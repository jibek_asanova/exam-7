import React from 'react';
import {prices} from "../App";

import './Order.css';
import deleteIcon from "../assets/delete.png";

const Order = ({menu, onRemove}) => {
    let orders = [];
    for (let i = 0; i < menu.length; i++) {
        if(menu[i].count > 0) {
            orders.push(<div className="Order">
                <p>{menu[i].name}</p>
                <p>Price: {prices[menu[i].name]}</p>
                <p>x{menu[i].count}</p>
                <button onClick={() => onRemove(menu[i].id)} disabled={menu[i].count === 0} className="btn"><img src={deleteIcon} alt='deleteIcon' width='30px' className="click"/></button>
            </div>);
        }
    }
    return (
        <div>
            {orders.length ? orders : (<h4>No orders yet!<br/>Please add some items!
            </h4>)}
        </div>

    );
};

export default Order;